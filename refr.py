import sys, math
import unittest

## Функция расчета скалярного произведения двух векторов
# @param x1 х-координата первого вектора
# @param y1 у-координата первого вектора
# @param x2 х-координата второго вектора
# @param y2 у-координата второго вектора
# @return результат скалярного произведения двух векторов
def scalar(x1,y1,x2,y2):
	return x1*x2 + y1*y2 # скалярное произведение

## Функция расчета косого произведения двух векторов
# @param x1 х-координата первого вектора
# @param y1 у-координата первого вектора
# @param x2 х-координата второго вектора
# @param y2 у-координата второго вектора
# @return результат косого произведения двух векторов
def skew(x1,y1,x2,y2):
    return x1*y2 - x2*y1 #косое произведение

## Функция определения пересечения луча и отрезка
# @param x0 х-координата точки начала луча
# @param y0 у-координата точки начала луча
# @param dx параметр распространения луча вдоль оси х
# @param dy параметр распространения луча вдоль оси у
# @param x1 х-координата одного конца отрезка
# @param y1 у-координата одного конца отрезка
# @param x2 х-координата второго конца отрезка
# @param y2 у-координата второго конца отрезка
# @return (True,координаты точки пересечения), если пересечение есть. (False,None,None), если пересечения нет 
def R_LIntersection(x0,y0,dx,dy,x1,y1,x2,y2):
    if (skew(dx,dy,x1-x0,y1-y0)*skew(dx,dy,x2-x0,y2-y0)) > 0:
        return (False,None,None)
    if skew(dx,dy,x1-x0,y1-y0) == 0:
        return (True,x1,y1) #пересечение в первой точке
    if skew(dx,dy,x2-x0,y2-y0) == 0:
        return (True,x2,y2) #пересечениие во второй точке
    if (scalar(dx,dy,x1-x0,y1-y0) < 0) or (scalar(dx,dy,x2-x0,y2-y0) < 0):
        return (False,None,None)
    #есть пересечение, но не понятно в какой точке
    x = (x1+x2)/2
    y = (y1+y2)/2
    i = 0
    while(R_LIntersection(x0,y0,dx,dy,x1,y1,x,y)[0] != True) :
        i+=1
        x = (x1+x2)/2
        y = (y1+y2)/2
        if(i>=5):
            return (True,(x1+x2)/2,(y1+y2)/2)
        if  ((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)) < 10:
            return (True,(x1+x2)/2,(y1+y2)/2)
        if(R_LIntersection(x0,y0,dx,dy,x1,y1,x,y)[0] == False):
            x1 = x
            y1 = y
        else:
            x2 = x
            y2 = y    
    return R_LIntersection(x0,y0,dx,dy,x1,y1,x,y)   

## Функция расчета преломления
# @param X х-координата точки преломления
# @param Y у-координата точки преломления
# @param dx параметр распространения луча вдоль оси х
# @param dy параметр распространения луча вдоль оси у
# @param x1 х-координата начала отрезка
# @param y1 у-координата начала отрезка
# @param n1 показатель преломления первой среды
# @param n2 показатель преломления второй среды
# @return (параметры распространения преломленного луча вдоль осей х и у), если не было полного внутреннего отражения. (None,None), если произошло полное внутреннее отражение
def refraction(X,Y,dx,dy,x1,y1,n1,n2): 
    nx = y1-Y
    ny = X-x1
    lenn = math.sqrt(nx*nx+ny*ny)
    nx = nx/lenn
    ny = ny/lenn
    v1x = dx*n1/(math.sqrt(dx*dx+dy*dy))
    v1y = dy*n1/(math.sqrt(dx*dx+dy*dy))
    s = scalar(v1x,v1y,nx,ny)
    if s == 0:
            print("s = 0")
            return (v1x,v1y)
    k2 = (n2*n2 - n1*n1)/(s*s) + 1
    if (k2 < 0):
        return (None,None)
    k = math.sqrt(k2) - 1
    v2x = v1x + k*s*nx
    v2y = v1y + k*s*ny
    lenv2 = math.sqrt(v2x*v2x+v2y*v2y)
    v2x = v2x*n2/lenv2
    v2y = v2y*n2/lenv2
    return (v2x,v2y)

## Функция определения пересечения луча с внешней стороной линзы
# @param ray массив, соответсвующий лучу
# @param Points массив, соответствующий набору точек линзы
# @param up_x х-координата верхней точки линзы
# @param up_y у-координата верхней точки линзы
# @param down_x х-координата нижней точки линзы
# @param down_y у-координата нижней точки линзы
# @return (координаты точки пересечения; номер отрезка,с которым произошло пересечение)
def find_intersect(ray,Points,up_x,up_y,down_x,down_y):
    i = 1
    intersect = R_LIntersection(ray[0],ray[1],ray[2],ray[3],up_x,up_y,Points[0][0],Points[0][1])
    if intersect[0] == True:
        return (intersect[1],intersect[2],i)
    else:
        while(1):
            if i == 5:
                intersect = R_LIntersection(ray[0],ray[1],ray[2],ray[3],Points[i-1][0],Points[i-1][1],down_x,down_y)
                if (intersect[0] == True):
                        return(intersect[1],intersect[2],i-1)
                else:
                        return (False,None,None)
            intersect = R_LIntersection(ray[0],ray[1],ray[2],ray[3],Points[i-1][0],Points[i-1][1],Points[i][0],Points[i][1])
            if(intersect[0] == True):
                break
            i+=1
        return (intersect[1],intersect[2],i)

## Функция определения пересечения луча с внутренней стороной линзы
# @param ray массив, соответсвующий лучу
# @param Points массив, соответствующий набору точек линзы
# @param up_x х-координата верхней точки линзы
# @param up_y у-координата верхней точки линзы
# @param down_x х-координата нижней точки линзы
# @param down_y у-координата нижней точки линзы
# @return (координаты точки пересечения; номер отрезка,с которым произошло пересечение)
def find_second_intersect(ray,Points,up_x,up_y,down_x,down_y):
    i = 6
    intersect = R_LIntersection(ray[0],ray[1],ray[2],ray[3],up_x,up_y,Points[5][0],Points[5][1])
    if intersect[0] == True:
        return (intersect[1],intersect[2],i)
    else:
        while(1):
            if i == 10:
                intersect = R_LIntersection(ray[0],ray[1],ray[2],ray[3],Points[i-1][0],Points[i-1][1],down_x,down_y)
                if (intersect[0] == True):
                        return(intersect[1],intersect[2],i-1)
                else:
                        return (False,None,None)
            intersect = R_LIntersection(ray[0],ray[1],ray[2],ray[3],Points[i-1][0],Points[i-1][1],Points[i][0],Points[i][1])
            if(intersect[0] == True):
                break
            i+=1
        return (intersect[1],intersect[2],i)

points = ((170,175),(135,225),(100,300),(135,375),(170,425),(235,175),(270,225),(300,300),(270,375),(235,425))
class Testing(unittest.TestCase):

        
        ## Тестирование функции scalar
        def test_scalar(self):
                self.assertEqual(scalar(1,0,0,1),0)
                self.assertEqual(scalar(10,5,-2,3),-5)
        
        ## Тестирование функции skew      
        def test_skew(self):
                self.assertEqual(skew(1,0,0,1),1)
                self.assertEqual(skew(10,5,-2,3),40)
                
        ## Тестирование функции R_LIntesection
        def test_R_LIntersection(self):
                self.assertFalse(R_LIntersection(10,10,1,0,15,5,30,5)[0])
                self.assertEqual(R_LIntersection(10,10,1,0,15,5,15,15),(True,15,10))
                self.assertFalse(R_LIntersection(10,10,1,-1,15,8,15,10)[0])
                self.assertEqual(R_LIntersection(10,10,1,-1,15,5,15,10),(True,15,5))
                
        ## Тестирование функции refraction
        def test_refraction(self):
                self.assertEqual(refraction(15,10,1,0,15,5,1,3),(3,0))
                self.assertAlmostEqual(refraction(15,10,1,-1,15,5,1,1)[0],1/math.sqrt(2))
                self.assertAlmostEqual(refraction(15,10,1,-1,15,5,1,1)[1],-1/math.sqrt(2))
                
        ## Тестирование функции find_intersect
        def test_find_intersect(self):
                self.assertFalse(find_intersect((0,50,1,0),points,200,100,200,500)[0])
                self.assertFalse(find_intersect((0,550,1,0),points,200,100,200,500)[0])
                self.assertEqual(find_intersect((0,225,1,0),points,200,100,200,500)[0],135)
                self.assertEqual(find_intersect((0,125,1,0),points,200,100,200,500)[2],1)
                self.assertEqual(find_intersect((0,475,1,0),points,200,100,200,500)[2],4)
                
        ## Тестирование функции find_second_intersect
        def test_find_second_intersect(self):
                self.assertFalse(find_second_intersect((0,50,1,0),points,200,100,200,500)[0])
                self.assertFalse(find_second_intersect((0,550,1,0),points,200,100,200,500)[0])
                self.assertEqual(find_second_intersect((0,225,1,0),points,200,100,200,500)[0],270)
                self.assertEqual(find_second_intersect((0,125,1,0),points,200,100,200,500)[2],6)
                self.assertEqual(find_second_intersect((0,475,1,0),points,200,100,200,500)[2],9)
        

if __name__ == '__main__':
        unittest.main()

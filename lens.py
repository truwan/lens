from PyQt4 import QtGui,QtCore
from PyQt4.QtCore import QPointF
import sys, math, copy
from scipy import optimize 
import refr
width = 600
lenght = 1200

## Целевая функция
# @param x_points список х-координат точек, которые нужно подобрать
# @param args(список лучей; радиус; угол; х-коодината положения линзы; у-координата положения линзы; показатель преломления; список у-координат точек, которые нужно подбрать)
# @return сумма квадратов отклонений точек пересечения лучей после прохода линзы с главной оптической осью от желаемого фокуса
def func(x_points,*args):
    Rays = args[0]
    rays = copy.deepcopy(Rays)
    radius = args[1]
    phi = args[2]
    X0 = args[3]
    Y0 = args[4]
    n = args[5]
    focus = args[6][0]
    y_points =args[7]
    y_step = (radius*math.sin(phi))
    x_step = (radius - radius*math.cos(phi))/3
    summ = 0
    points = []
    for i in range(len(x_points)): 
        points.append((x_points[i],y_points[i]))
    for i in range(len(rays)):
        coord = refr.find_intersect(rays[i],points,X0,Y0-y_step,X0,Y0+y_step)
        if coord[0] == False:
            X = -10000
            summ += (X-focus)*(X-focus)
            continue
        j = coord[2]
        if ((coord[0] == points[j][0]) and (coord[1] == points[j][1])):               
            if(j==0 or j==1):
                j=1
            else:
                j-=1
        v2=refr.refraction(coord[0],coord[1],rays[i][2],rays[i][3],points[j][0],points[j][1],1,n)
        if v2[0] == None:
            X = -10000
            summ+=(X-focus)*(X-focus)
            continue
        rays[i][0] = coord[0]
        rays[i][1] = coord[1]
        rays[i][2] = v2[0]
        rays[i][3] = v2[1]
        coord = refr.find_second_intersect(rays[i],points,X0,Y0-y_step,X0,Y0+y_step)
        if coord[0] == False:
            X = -10000
            summ += (X-focus)*(X-focus)
            continue
        j = coord[2]
        if ((coord[0] == points[j][0]) and (coord[1] == points[j][1])):                 
            if(j==5 or j==6):
                j=6
            else:
                j-=1
        v2=refr.refraction(coord[0],coord[1],rays[i][2],rays[i][3],points[j][0],points[j][1],n,1)
        if v2[0] == None:
            X = -10000
            summ+=(X-focus)*(X-focus)
            continue
        rays[i][0] = coord[0]
        rays[i][1] = coord[1]
        rays[i][2] = v2[0]
        rays[i][3] = v2[1]
        if (rays[i][3] == 0):
            X = 10000
        else:
            X = rays[i][0] + abs((width/2 - rays[i][1])*rays[i][2]/rays[i][3])
        summ += (X-focus)*(X-focus)
    print('summ = ',summ)
    return summ     
     
class MyWidget(QtGui.QWidget):
    ## Конструктор
    def __init__(self):
        super().__init__()
        self.setGeometry(100,100,lenght,width)
        self.setWindowTitle('PROJECT LENS')
        self.Points = []
        self.focus = []
        self.Rays = []
        self.PaintFlag = 0
        self.flag = 'none'
        self.FocPoints = []
    # ADD button    
        self.add_but = QtGui.QPushButton('ADD',self)
        self.add_but.resize(self.add_but.sizeHint())
        self.add_but.move(lenght/5,width-50)
        self.add_but.clicked.connect(self.Add_Lens)
    # SET FOCUS button
        self.setFoc_but = QtGui.QPushButton('SET FOCUS',self)
        self.setFoc_but.resize(self.setFoc_but.sizeHint())
        self.setFoc_but.move(2*lenght/5,width-50)
        self.setFoc_but.clicked.connect(self.Set_Focus)
    # SHOW button
        self.show_but = QtGui.QPushButton('SHOW',self)
        self.show_but.resize(self.show_but.sizeHint())
        self.show_but.move(3*lenght/5,width-50)
        self.show_but.clicked.connect(self.show_action)
    # START button
        self.start_but = QtGui.QPushButton('START',self)
        self.start_but.resize(self.start_but.sizeHint())
        self.start_but.move(4*lenght/5,width-50)
        self.start_but.clicked.connect(self.start_action)

    ## Обработка нажатий мыши
    # @param q объект типа QMouseEvent
    def mousePressEvent(self,q):
    # ADD
        if self.flag == 'add':
            x_step = (self.radius - self.radius*math.cos(self.phi))/3
            y_step = self.radius*math.sin(self.phi)/3
            self.X0 = q.x()
            self.Y0 = width/2 #q.y() фиксированная ось
            self.Points.append((self.X0-x_step,self.Y0-y_step*2))
            self.Points.append((self.X0-x_step*2,self.Y0-y_step))
            self.Points.append((self.X0-x_step*3,self.Y0))
            self.Points.append((self.X0-x_step*2,self.Y0+y_step))
            self.Points.append((self.X0-x_step,self.Y0+y_step*2))
            self.Points.append((self.X0+x_step,self.Y0-y_step*2))
            self.Points.append((self.X0+x_step*2,self.Y0-y_step))
            self.Points.append((self.X0+x_step*3,self.Y0))
            self.Points.append((self.X0+x_step*2,self.Y0+y_step))
            self.Points.append((self.X0+x_step,self.Y0+y_step*2))
            self.flag = 'none'
            self.update()
    # FOCUS
        if self.flag == 'focus':
            self.focus = (q.x(),width/2) #q.y()) фиксированная ось
            self.flag = 'none'
            self.update()
            
    ## Функция добавления линзы        
    def Add_Lens(self):
        #print('ADD')
    # Радиус
        self.radius, ok = QtGui.QInputDialog.getDouble(self, 'input radius', 'R = ')
        if ok == False:
            print('something is wrong!')
        print('radius = ',self.radius)
    # Угол
        self.phi, ok = QtGui.QInputDialog.getText(self, 'input angle', 'Angle in rad = ')
        if ok == False:
            print('something is wrong!')
        self.phi = float(self.phi)
        print('phi = ',self.phi)
    # Показатель преломления
        self.n, ok = QtGui.QInputDialog.getText(self, 'input n', 'n = ')
        if ok == False:
            print('something is wrong!')
        self.n = float(self.n)
        print('n = ',self.n)
##    #
##        self.radius = 500
##        self.phi = 0.35
##        self.n = 2
##    #
        self.flag = 'add'
        h = self.radius*math.sin(self.phi)/5
        for i in range (-4,5):
            self.Rays.append([0,width/2 + i*h,1,0])
            
    ## Функция для показа хода лучей    
    def show_action(self):
        self.PaintFlag = 1
        self.update()
        
    ## Функция начала натройки системы линз
    def start_action(self):
        self.rays = copy.deepcopy(self.Rays) 
        self.x_points = []
        self.y_points = []
        for i in range(len(self.Points)):
                self.x_points.append(self.Points[i][0])
                self.y_points.append(self.Points[i][1])
        arguments = (self.rays,self.radius,self.phi,self.X0,self.Y0,self.n,self.focus,self.y_points)
        self.result = optimize.fmin(func,self.x_points,args = arguments,maxiter=400,full_output=True)[0]
        self.x_points = self.result
        print("your summ = ",func(self.x_points, *arguments))
        self.Points = list(zip(self.result,self.y_points))
        self.update()


    ## Функция задания желаемого фокусного расстояния            
    def Set_Focus(self):
        self.flag = 'focus'

    ## Функция рисования главной оптической оси  
    def draw_main_axis(self):
        p = QtGui.QPainter()
        p.begin(self)
        p.setPen(QtGui.QPen(QtGui.QColor(128,128,128),1))
        p.drawLine(QPointF(0,width/2),QPointF(lenght,width/2))
        p.end()

    ## Функция рисования линзы
    # @param x х-координата положения линзы
    # @param y у-координата положения линзы
    # @param Points список точек, по которым производится настройка
    # @param radius радиус линзы
    # @param phi угол раствора линзы
    def draw_lens(self,x,y,Points,radius, phi):
        y_step = (radius*math.sin(phi))/3
        x_step = (radius - radius*math.cos(phi))/3
        p = QtGui.QPainter()
        p.begin(self)
        p.setPen(QtGui.QPen(QtGui.QColor(0,0,128),2))
        p.drawLine(QPointF(x,y-3*y_step),QPointF(Points[0][0],Points[0][1]))
        p.drawLine(QPointF(x,y-3*y_step),QPointF(Points[5][0],Points[5][1]))
        p.drawLine(QPointF(x,y+3*y_step),QPointF(Points[4][0],Points[4][1]))
        p.drawLine(QPointF(x,y+3*y_step),QPointF(Points[9][0],Points[9][1]))
        p.setPen(QtGui.QPen(QtGui.QColor(255,0,0),5))
        p.drawPoint(QPointF(Points[0][0],Points[0][1]))
        for i in range (9):
            p.setPen(QtGui.QPen(QtGui.QColor(255,0,0),5))
            p.drawPoint(QPointF(Points[i+1][0],Points[i+1][1]))
            p.setPen(QtGui.QPen(QtGui.QColor(0,0,128),2))
            if i == 4:
                continue
            p.drawLine(QPointF(Points[i][0],Points[i][1]),QPointF(Points[i+1][0],Points[i+1][1]))
        p.end()

    ## Функция рисованния желаемого фокусного расстояния
    def drawFocus(self,point):
        p = QtGui.QPainter()
        p.begin(self)
        p.setPen(QtGui.QPen(QtGui.QColor(0,0,0),5))
        p.drawPoint(QPointF(point[0],point[1]))
        p.end()

    ## Функция рисования хода лучей через систему линз
    def drawIntersection(self,rays,points,radius,phi,X0,Y0,n):
        y_step = (radius*math.sin(phi))
        x_step = (radius - radius*math.cos(self.phi))/3
        p = QtGui.QPainter()
        p.begin(self)
        self.FocPoints = [] 
        for i in range(len(rays)):
            p.setPen(QtGui.QPen(QtGui.QColor(255,179,0),3))
            p.drawPoint(QPointF(rays[i][0],rays[i][1]))
            coord = refr.find_intersect(rays[i],points,X0,Y0-y_step,X0,Y0+y_step)
            p.drawLine(QPointF(rays[i][0],rays[i][1]),QPointF(coord[0],coord[1]))
            j = coord[2]
            if ((coord[0] == points[j][0]) and (coord[1] == points[j][1])):                 
                if(j==0 or j==1):
                    j=1
                else:
                    j-=1
            v2=refr.refraction(coord[0],coord[1],rays[i][2],rays[i][3],points[j][0],points[j][1],1,n)
            rays[i][0] = coord[0]
            rays[i][1] = coord[1]
            rays[i][2] = v2[0]
            rays[i][3] = v2[1]
            coord = refr.find_second_intersect(rays[i],points,X0,Y0-y_step,X0,Y0+y_step)
            x = coord[0]
            y = coord[1]
            p.drawLine(QPointF(rays[i][0],rays[i][1]),QPointF(x,y))
            j = coord[2]
            if ((coord[0] == points[j][0]) and (coord[1] == points[j][1])):                 
                if(j==5 or j==6):
                    j=6
                else:
                    j-=1
            v2=refr.refraction(coord[0],coord[1],rays[i][2],rays[i][3],points[j][0],points[j][1],n,1)
            rays[i][0] = coord[0]
            rays[i][1] = coord[1]
            rays[i][2] = v2[0]
            rays[i][3] = v2[1]
            if (rays[i][3] == 0 or rays[i][3] == None):
                if(rays[i][3] == 0):
                    X = 100000
                else:
                    X = -100000
            else:
                X = rays[i][0] + abs((width/2 - rays[i][1])*rays[i][2]/rays[i][3])
            self.FocPoints.append(X)
            p.drawLine(QPointF(rays[i][0],rays[i][1]),QPointF(self.FocPoints[i],width/2))
            p.setPen(QtGui.QPen(QtGui.QColor(0,0,255),3))
            p.drawPoint(QPointF(self.FocPoints[i],width/2))
        print("FOCPOINTS: ",self.FocPoints)
        p.end()

    ## Рисование
    #@param e объект типа QPaintEvent
    def paintEvent(self,e):
        self.draw_main_axis()
        if (len(self.Points) != 0):
            self.draw_lens(self.X0,self.Y0,self.Points,self.radius,self.phi)
            if self.PaintFlag == 1:
                self.rays = copy.deepcopy(self.Rays)
                self.drawIntersection(self.rays,self.Points,self.radius,self.phi,self.X0,self.Y0,self.n)
                self.PaintFlag = 0
        if (len(self.focus) != 0):
            self.drawFocus(self.focus)
            
    
   

def main():
    app = QtGui.QApplication(sys.argv)
    w = MyWidget()
    w.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
    
    
